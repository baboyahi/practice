package April;

public class Day5 {
   public static int solution(int a, int b, int c) {
       if (a == b && b == c) {
           return (a + b + c) * (a * a + b * b + c * c) * (a * a * a + b * b * b + c * c * c);
       }
       else if (a != b && b != c && c != a) {
           return a + b + c;
       }
       else {
           return (a + b + c) * (a * a + b * b + c * c);
       }
   }
   
/**
 * 정수가 담긴 리스트 num_list가 주어질 때, 모든 원소들의 곱이 모든 원소들의 합의 제곱보다 작으면 1을 크면 0을 return하도록 solution 함수를 완성해주세요.
 * @param num_list
 * @return
 */
   
   public int solution(int[] num_list) {
	   int answer=0;
       int multiply = 0;
       int sum = 0;
       for (int i = 0; i < num_list.length; i++) {
       multiply*=num_list[i];
       sum+=num_list[i];
	}
       if(multiply<sum*sum) {
    	   return 1;
       }else if(multiply>sum*sum){
    	   return 0;
       }
       return answer;
   }


public static void main(String[] args) {
	int solution = solution(1, 2, 4);
 System.out.println(solution);
}
}
