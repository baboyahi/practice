package April;

import java.util.Arrays;
import java.util.Iterator;

public class Day6 {
	public static int solution(int n, String control) {
		for (int i = 0; i < control.length(); i++) {
			switch (control.charAt(i)) {
			case 'w':
				n += 1;
				break;
			case 's':
				n -= 1;
				break;
			case 'd':
				n += 10;
				break;
			case 'a':
				n -= 10;
				break;
			}
		}
		return n;
	}



	public static String solution(int[] numLog) {
		String answer = "";
		for (int i = 1; i < numLog.length; i++) {
			switch (numLog[i] - numLog[i - 1]) {
			case 1:
				answer += "w";
				break;
			case -1:
				answer += "s";
				break;
			case -10:
				answer += "a";
				break;
			case 10:
				answer += "d";
				break;
			default:
				break;
			}
		}
		return answer;
	}


	/*
	 * 정수 배열 arr와 2차원 정수 배열 queries이 주어집니다. queries의 원소는 각각 하나의 query를 나타내며, [s, e,
	 * k] 꼴입니다.
	 * 
	 * 각 query마다 순서대로 s ≤ i ≤ e인 모든 i에 대해 k보다 크면서 가장 작은 arr[i]를 찾습니다.
	 * 
	 * 각 쿼리의 순서에 맞게 답을 저장한 배열을 반환하는 solution 함수를 완성해 주세요. 단, 특정 쿼리의 답이 존재하지 않으면 -1을
	 * 저장합니다.
	 */
	
	
    public static int[] solution(int[] arr, int[][] queries) {
            int[] answer = new int[queries.length];

            for (int i = 0; i < queries.length; i++) {
                int s = queries[i][0];
                int e = queries[i][1];
                int k = queries[i][2];
                int min = Integer.MAX_VALUE;

                for (int j = s; j <= e; j++) {
                    if (arr[j] > k && arr[j] < min) {
                        min = arr[j];
                    }
                }

                answer[i] = (min != Integer.MAX_VALUE) ? min : -1;
            }

            return answer;
        }
	 
	 
	 
	 

	public static void main(String[] args) {
		 int[] numLog = {3, 4, 5, 6, 16, 17, 18, 19};
		System.out.println(solution(10, "wasd"));
		System.out.println(solution(numLog));
		int[] arr = {0, 1, 2, 4, 3};
        int[][] queries = {{0, 4, 2}, {0, 3, 2}, {0, 2, 2}};
        int[] result = solution(arr, queries);
        System.out.println(Arrays.toString(result));
	}
}
