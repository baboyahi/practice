package march;

public class Day3 {

	public static void main(String[] args) {
		String str1 = "aaaaa";
		String str2 = "bbbbb";

		StringBuilder result = new StringBuilder();
		int length = Math.min(str1.length(), str2.length());

		for (int i = 0; i < length; i++) {
			result.append(str1.charAt(i)).append(str2.charAt(i));
		}

		System.out.println(result.toString());
		
		
		String[] arr = {"h", "e", "l", "l", "o"};
		String result2 = String.join("a", arr);
		System.out.println(result2);  // 출력: haealalaoa
		
		int[] numbers = {1, 2, 3, 4, 5};
		String numbersString = numbers.toString();
		System.out.println(numbersString); // 출력: [I@50d0686
	}
	
	

}
