package march;

public class Day4 {
	public static int solution(int n) {
		int number = 0;

		if (n % 2 != 0) {
			for (int i = 1; i <= n; i += 2) {
				number += i;
			}
			return number;
		}
		for (int i = 0; i <= n; i += 2) {
			number += (i * i);
		}
		return number;
	}


	    public static String solution(String code) {
	        StringBuilder ret = new StringBuilder();
	        int mode = 0;

	        for (int idx = 0; idx < code.length(); idx++) {
	            if (mode == 0) {
	                if (code.charAt(idx) != '1') {
	                    if (idx % 2 == 0) {
	                        ret.append(code.charAt(idx));
	                    }
	                } else {
	                    mode = 1;
	                }
	            } else {
	                if (code.charAt(idx) != '1') {
	                    if (idx % 2 == 1) {
	                        ret.append(code.charAt(idx));
	                    }
	                } else {
	                    mode = 0;
	                }
	            }
	        }

	        if (ret.length() == 0) {
	            return "EMPTY";
	        } else {
	            return ret.toString();
	        }
	    }


	public static void main(String[] args) {
		int result = solution(7);
		String result2 = solution("abc1abc1abc");
		System.out.println(result2);
	}
}
	

